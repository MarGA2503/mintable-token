const path = require("path");
require("dotenv").config({path: "./.env"});
const HDWalletProvider = require("@truffle/hdwallet-provider");
const MetaMaskAccountIndex = 0;

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    development: {
      port: 9545
    },
    ganache_local: {
      provider: () =>
        new HDWalletProvider(process.env.MNEMONIC, "http://127.0.0.1:8545",MetaMaskAccountIndex),
        network_id: 1337 // Any network (default: none)
    },
    rinkeby_infura: {
      provider: () =>
        new HDWalletProvider(process.env.MNEMONIC, "https://rinkeby.infura.io/v3/ffa60321cbbd4cfda3352014c556e3c3",MetaMaskAccountIndex),
        network_id: 4 // Any network (default: none)
    },
    goerli_infura: {
      provider: function() {
        return new HDWalletProvider(process.env.MNEMONIC, "https://goerli.infura.io/v3/ffa60321cbbd4cfda3352014c556e3c3", MetaMaskAccountIndex)
      },
      network_id: 5
    },

    ropsten_infura: {
      provider: () =>
        new HDWalletProvider(process.env.MNEMONIC, "https://ropsten.infura.io/v3/ffa60321cbbd4cfda3352014c556e3c3",MetaMaskAccountIndex),
        network_id: 3 // Any network (default: none)
    }
  },
  compilers: {
    solc: {
      version: "0.6.1"
    }
  }
};
