# Mintable Token

This is the Final project for the Ethereum Blockchain Developer Bootcamp With Solidity (2022) course in Udemy.

https://www.udemy.com/course/blockchain-developer/

## Deployed contract addresses for interaction
Rinkeby : 0x579cdf56489b5Ddd6388574f284Cb7BE10b77C3b

## Installation
Download and install Metamask.

Create a .env file on root folder and add:

MNEMONIC = [YOUR_MNEMONIC]

Install and run the project.

```bash
npm install
cd client
npm install
npm run start

```

In the root directory of the project run:
```bash
 truffle migrate --network [NETWORK_NAME] --reset

```
Select NETWORK_NAME from truffle-config.js file.

## Usage
Replace default Infura api keys given in the course with your own in truffle-config.js file.

Make sure Metamask is connected to the current test network before interacting with the contract.

Deployer address (the first in Metamask) needs to allow buyer address before purchasing any tokens.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
