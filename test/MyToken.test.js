const Token = artifacts.require("MyToken");
const assert = require('assert');

const chai = require("./setupchai.js");
const BN = web3.utils.BN;
const expect = chai.expect;

require("dotenv").config({path: "../.env"});

contract("Token Test", async (accounts) => {

    const [deployerAccount, recipient, anotherAccount] = accounts;

    beforeEach(async() => {
        this.myToken = await Token.new();
    })

    it("all tokens should be in my account", async () => {
        let instance = this.myToken;
        let initialSupply = 0;
        let totalSupply = await instance.totalSupply();
        let balance = await instance.balanceOf(accounts[0]);
        assert.equal(balance.valueOf(), initialSupply.valueOf(), "The balance was not the same");
        return expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(totalSupply);
    })

    it("is possible to mint tokens", async() => {
        const mintquantity = 100;
        let instance = this.myToken;
        //let minted = await instance.mint(deployerAccount,mintquantity);
        let minted = await instance.issueToken(mintquantity)
        assert(minted);
        expect (instance.mint(deployerAccount, mintquantity)).to.eventually.be.fulfilled;
        let totalSupply = await instance.totalSupply();
        //expect(instance.totalSupply()).to.eventually.be.a.bignumber.equal(mintquantity);
        assert.equal(totalSupply.valueOf(), mintquantity.valueOf(), "The balance was not the same");
        return expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(new BN(mintquantity));



    });

    it("is possible to send tokens between accounts", async() => {
        const sendTokens = 1;
        const mintquantity = 100;
        let instance = this.myToken;
        let minted = await instance.issueToken(mintquantity)
          assert(minted);
        let totalSupply = await instance.totalSupply();
         expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(totalSupply);
        let transferred = await instance.transfer(recipient, sendTokens);
        //   assert(transferred);
        //expect(instance.transfer(recipient, sendTokens)).to.eventually.be.fulfilled;
       expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(totalSupply.sub(new BN(sendTokens)));
       return expect(instance.balanceOf(recipient)).to.eventually.be.a.bignumber.equal(new BN(sendTokens));
    });

    // it("is not possible to send more tokens than available in total", async () => {
    //     let instance = this.myToken;
    //     let balanceOfDeployer = await instance.balanceOf(deployerAccount);
    //
    //     expect(instance.transfer(recipient, new BN(balanceOfDeployer+1))).to.eventually.be.rejected;
    //
    //     return expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(balanceOfDeployer);
    // });

});
